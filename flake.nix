{
  description = "A tiny command-line process orchestrator";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-20.03;

  outputs = { self, nixpkgs }: {

    defaultPackage.x86_64-linux =
      with import nixpkgs { system = "x86_64-linux"; };
      rustPlatform.buildRustPackage {
        pname = "orq";
        version = "0.1.1";
        src = self;
        cargoSha256 = "0hkjy4a5fyj1bmk99g5kap2qpha7vddxw64zl8zkhx1vmklyz423";
      };
  };
}
