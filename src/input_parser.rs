use failure::{format_err, Error};
use std::env;
use tokio::{
    fs::File,
    io::{stdin, AsyncBufReadExt, AsyncRead, BufReader},
    stream::{Stream, StreamExt},
};

pub const USAGE: &str = "
Runs list of shell commands in parallel and interleaves their command line output

Usage:

    orq [<file>]

Options:

    The file should contain a list of commands to be executed. If no file is specified, the
    commands are read from stdin instead.
";

pub async fn retrieve_source() -> Result<impl AsyncRead, Error> {
    let args: Vec<_> = env::args().collect();
    match args.len() {
        1 => Ok(Box::new(stdin()) as Box<dyn AsyncRead + Unpin + Send + 'static>),
        2 => {
            Ok(Box::new(File::open(&args[1]).await?)
                as Box<dyn AsyncRead + Unpin + Send + 'static>)
        }
        _ => Err(format_err!("Too few arguments to command")),
    }
}

pub fn read_commands(source: impl AsyncRead) -> impl Stream<Item = String> {
    BufReader::new(source).lines().filter_map(|x| x.ok())
}
